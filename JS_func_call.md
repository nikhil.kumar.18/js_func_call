## Pass by reference vs Pass by value in JavaScript

Hello folks, I am new to JavaScript programming and this language never fails to amaze me. I have little bit programming experience in C++. The only major difference I noticed in these two languages (till now) is call by reference and call by value. Lets learn more about pass by value and pass by reference through an example.

 If your friend asks to borrow a book from you and you can't afford to lose this book or any damage done to this book for some reason. So, in that situation you can take a xerox copy of the book and give to your friend. In that case if the xerox copy gets burned or damaged then it doesn't affects the original book. This exact thing happens in pass by value. **We take a copy of the value and pass it into the function. So anything that happens to the copy doesn't affects the original value.**

**In pass by reference we share the address of the value. Using that address our function can change our original variable value.**

Lets look at an example in JavaScript.

JavaScript

```
 let val=0  
 console.log(“Before Call :- “,val)  
 function convert(val)  
 {  
    val+=10;  
 }  
 convert(val)  
 console.log(“After Call:- “,val)  
```
```
 Output:-  
 Before Call :- 0  
 After Call :- 0
```

In the above examples when we pass val we only share its value not address. So the changes made to its passed value doesn't apply to original value.

As we can see from above example how value passed into js function didn’t change. The reason behind this is that all the primitive data types such as number,string and boolean etc are passed by value in JavaScript.


Now lets look at an example using arrays in Javascript
```
 let arr=[1,2,3]  
 console.log(“Before Call :- “,arr)  
 function add_hello(other_arr)  
 {  
    other_arr.push(“hello”);  
 }  
 add_hello(arr)  
 console.log(“After Call:- “,arr)  
```
```
 Output:-  
 Before Call :- [1,2,3]  
 After Call :- [1,2,3,”hello”]  
```
The arrays are not primitive data types, hence they are passed by reference not by value. In the above example changes made in other_arr also affects the original arr.

Example with objects:-
```
 let obj={Name:"Nikhil",Programming_language:"JavaScript"}  
 function add_topic(other_obj)  
 {  
    other_obj[Topic]="Objects"  
 }  
 console.log("Before Call:- ",obj);  
 add_topic(obj);  
 console.log("After Call:- ",obj);  
```
```
 Output:-  
 Before Call:- {Name:"Nikhil",Programming_language:"JavaScript"}  
 AfterCall:- {Name:"Nikhil",Programming_language:"JavaScript",Topic:"Objects"}  
```
In the above example when we pass the obj in the function we actually pass obj's address not its value, So the changes made to other_obj will affect obj as both the objects are same(they have same address).

**The data types such as arrays,objects or anything that is not a primitive data type is passed by reference in JavaScript.**


Lets look at another example

```
 let arr_1=[1,2,3]  
 let arr_2=[1,2,3]  
 console.log(arr_1===arr_2) //outputs false because arrays or objects are compared by reference not value  
 let val_1=100  
 let val_2=100  
 console.log(val_1===val_2) /*outputs true because numbers (primitive data types) are compared by values */  
```
As shown above, both arrays arr_1 and arr_2 have same values but different address. So when we try to equate them using === it shows false because arrays and objects are compared by references not values.

In the other hand both val_1 and val_2 have same values and they are primitive data types so it shows true when we  try to equate them using === it shows true because primitive data types are compared by values not references.

#### summary:-

In JavaScript objects and arrays are passed by reference while primitive data types such as numbers,strings and booleans etc are passed by values.
